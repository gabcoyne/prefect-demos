import random
from prefect import flow, task
import modin.pandas as pd
import json
from google.cloud import bigquery
from google.cloud.bigquery import dbapi
from google.oauth2 import service_account
import ponder
from modin.config import LogMode
from prefect_gcp import GcpCredentials
from prefect.artifacts import create_table_artifact
from datetime import datetime
from prefect.blocks.system import String

LogMode.disable()


@task(name="Load Data to Ponder", log_prints=True)
def load_data():
    df = pd.read_csv(
        "https://raw.githubusercontent.com/ponder-org/ponder-datasets/main/yellow_tripdata_2015-01.csv",
        header=0,
    )
    return df


@task(name="Filter Passenger Charges", log_prints=True)
def filter_data(df):
    df = df[df["PASSENGER_COUNT"] > 0]
    df = df.drop(columns=["CONGESTION_SURCHARGE", "AIRPORT_FEE"])
    return df


@task(name="Process Tip Amount", log_prints=True)
def process_tips(df):
    return df.loc[df["TIP_AMOUNT"] > 0]


@task(name="Analyze Passengers", log_prints=True, retries=10, retry_jitter_factor=1)
def analyze_data(df):
    if random.randint(1, 2) == 1:
        df_passcount = df.groupby(["PASSENGER_COUNT"]).count()["VENDORID"]
        return df_passcount.describe()
    else:
        raise ValueError("Randomly failed")


@task(name="Create Artifact")
def create_artifact(key: str, table: list, description: str):
    create_table_artifact(key=key, table=table, description=description)


def describe_df(df):
    return df.describe().to_dict("records")


@flow(log_prints=True)
def taxi_pipeline():
    # Initialize Ponder
    ponder.init()

    # Grab credentials from prefect bloc
    # Setup BigQuery Connection using Prefect Block
    bigquery_con = dbapi.Connection(
        GcpCredentials.load("ponder-block").get_bigquery_client()
    )

    # Configure Ponder
    ponder.configure(
        bigquery_dataset="PONDER",
        default_connection=bigquery_con,
        row_transfer_limit=15_000,
    )

    # Data Prep
    df = load_data()
    df_filtered = filter_data(df)

    df_tips = process_tips(df_filtered)

    create_artifact(
        key="tip-amount",
        table=describe_df(df_tips),
        description="Tip Amounts from NYC Taxi Rides",
    )
    analysis = analyze_data(df_tips)


# To run the flow
if __name__ == "__main__":
    taxi_pipeline()
